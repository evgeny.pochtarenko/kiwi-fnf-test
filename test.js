const schema = {
  type: 'object',
  properties: {
    key: { type: 'string' },
    call: {
      type: 'object',
      properties: {
        senderId: { type: 'string' },
        channelId: { type: 'string' },
        type: { type: 'string' },
        agoraChannelId: { type: 'string' },
      }
    },
    invite: {
      type: 'object',
      properties: {
        id: { type: 'string' },
        sender: { type: 'object' },
        status: { type: 'string' },
        recipient: { type: 'object' },
        type: { type: 'string' },
      }
    },
  },
};

const {
  createCompressionTable,
  compressObject,
  decompressObject
} = require('jsonschema-key-compression');

const payload = {
  key: 'FNF^2222222222',
  call: {
    channelId: "C^I^136014-FNF^2222222222",
    type: "MissedCall",
    messageId: 469,
    agoraChannelId: "C^I^136014-FNF^2222222222_469",
    senderId: "I^136014",
  },
  invite: {
    id: 'qweqweqwe',
    sender: {
      first: 'first',
      last: 'last'
    },
    status: 'test_status',
    type: 'aqwe'
  }
};


const compressionTable = createCompressionTable(schema, 'S');

console.log(compressionTable);


console.log(compressObject(compressionTable, payload))

console.log(decompressObject(compressionTable, payload))