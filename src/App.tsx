import React, { useEffect, useState } from "react";
import "./App.css";
import {Auth} from "aws-amplify";
import {
  AddVideoCallThumbnailCmd,

  AppSource,

  KiwiProvider,
  KiwiSvc,
  OnUpdateForUserSub,

  UpdateUserNotificationDataCmd,


} from "kiwi-sdk";
import {
  Button,
  Divider,
  FormControl,
  Grid,
  InputLabel,
  List,
  ListItem,
  MenuItem,
  Select,
  TextField,
} from "@mui/material";
import classes from "./App.module.scss";
import Agora from "./Agora";
import { config } from "./config";
import { initializeApp } from "firebase/app";
import { getMessaging, getToken, onMessage } from "firebase/messaging";
import { AgencyProductType } from "kiwi-sdk/dist/types/agency";
import { kiwiTokenSvc } from "./token-handler";
import { decode } from "base64-arraybuffer";


const notifications = [
  "StartCall",
  "TnCAccepted",
  "TnCDeclined",
  "TnCReading",
  "EndCall",
  "ConnectedToCall",
  "Terminated",
  "Rejected",
  "MissedCall",
  "TimeSlotEnded",
  "DurationExpired",
  "RecordingError",
];

// @ts-ignore
const UserProfile = ({ handleIdUpdated, user, handleSendNotification }) => {
  const [id, setId] = useState("");
  const handleFormSubmit = (e: any) => {
    e.preventDefault();
    handleIdUpdated(id);
  };

  const [selectedType, setType] = useState(notifications[0]);
  const [agoraChannel, setAgoraChannel] = useState("");

  const handleNotifcationSubmit = (e: any) => {
    e.preventDefault();
    const [channelId, messageId] = agoraChannel.split("_");
    handleSendNotification({
      channelId,
      agoraChannelId: agoraChannel,
      type: selectedType,
      messageId: Number(messageId),
      senderId: user.id,
    });
  };

  return (
    <>
      <div>
        <div className={classes.info}>
          Id:{" "}
          {`${
            user ? `${user.firstName} ${user.lastName}` : "--not logged in--"
          }`}
        </div>
        <div className={classes.info}>
          Name: {`${user ? `${user.id}` : "--not logged in--"}`}
        </div>
        <div className={classes.info}>
          Email: {`${user ? `${user.email}` : "--not logged in--"}`}
        </div>
        <form onSubmit={handleFormSubmit} className={classes.UserForm}>
          <TextField
            value={id}
            onChange={(e) => {
              setId(e.target.value);
            }}
            id="outlined-basic"
            label="User Id"
            variant="outlined"
            style={{ marginBottom: "20px" }}
          />
          <Button type={"submit"} variant="contained">
            Set User
          </Button>
        </form>
      </div>
      <div>
        {user && (
          <form onSubmit={handleNotifcationSubmit} className={classes.UserForm}>
            <h3>Send Notification to Channel</h3>
            <FormControl fullWidth>
              <InputLabel id="demo-simple-select-label">Age</InputLabel>
              <Select
                labelId="demo-simple-select-label"
                id="demo-simple-select"
                value={selectedType}
                label="Age"
                onChange={(e) => {
                  setType(e.target.value);
                }}
              >
                {notifications.map((type) => (
                  <MenuItem key={type} value={type}>
                    {type}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
            <TextField
              value={agoraChannel}
              onChange={(e) => {
                setAgoraChannel(e.target.value);
              }}
              id="outlined-basic"
              label="agoraChannelId"
              variant="outlined"
              style={{ margin: "20px 0" }}
            />
            <Button type={"submit"} variant="contained">
              Send Notification
            </Button>
          </form>
        )}
      </div>
    </>
  );
};

// @ts-ignore
const CallSub = ({ user, provider }) => {
  console.log(!user?.id, user?.id);
  const [disabled, setDisabled] = useState(!user?.id);
  const [subLogs, setLogs] = useState([]);
  const [subCmd, setSubCmd] = useState(null);

  useEffect(() => {
    if (user?.id && provider) {
      setDisabled(false);
      const subCmd = OnUpdateForUserSub(user.id, (data: any) => {
        const callData = data?.call;
        console.log(data);
        if (callData) {
          // @ts-ignore
          setLogs((prevState) => [...prevState, callData]);
        }
      });
      // @ts-ignore
      setSubCmd(subCmd);
    }
  }, [user, provider]);

  const handleSubClicked = () => {
    provider.subscribe(subCmd);
    setDisabled(true);
  };

  return (
    <div className={classes.callSub}>
      <Button
        disabled={disabled}
        onClick={handleSubClicked}
        variant="contained"
      >
        Subscribe to call
      </Button>
      <List sx={{ bgcolor: "background.paper" }}>
        {subLogs.map((log, i) => {
          const { agoraChannelId, channelId, senderId, messageId, type } = log;
          return (
            <>
              <ListItem key={`${agoraChannelId}-${i}`} alignItems="flex-start">
                <div className={classes.listItem}>
                  <div>
                    <b>agoraChannelId:</b> {agoraChannelId}
                  </div>
                  <div>
                    <b>channelId:</b> {channelId}
                  </div>
                  <div>
                    <b>senderId:</b> {senderId}
                  </div>
                  <div>
                    <b>messageId:</b> {messageId}
                  </div>
                  <div>
                    <b>type:</b> {type}
                  </div>
                </div>
              </ListItem>
              <Divider key={`${agoraChannelId}-${i}-divider`} component="li" />
            </>
          );
        })}
      </List>
    </div>
  );
};

function App() {
  const [user, setUser] = useState(null);

  const [provider, setProvider] = useState(null);

  const [authLoading, setAuthLoading] = useState(undefined);



  useEffect( () => {

    const fetch = async () => {

      if (authLoading !== false || !provider) return;

      console.log(authLoading, provider)
      // @ts-ignore
      console.log(await provider.auth.getAuthenticatedUser())
/*
      const subCmd = OnUpdateForUserSub("test", (data) => {
        console.log({ data });
      });

      // @ts-ignore
      provider.subscribe(subCmd);
*/


      const kiwi = KiwiSvc(provider);

      const fnfInfo  = await kiwi.fnf.info({id: 'FNF^1111111111'})
      console.log({fnfInfo})
    }
    fetch();

  }, [authLoading, provider])

/*  const checkData = async (kiwiProvider: any) => {

    if (authLoading !== false) return;



  };*/

  const requestPermission = () => {
    console.log("Requesting permission...");
    Notification.requestPermission().then((permission) => {
      if (permission === "granted") {
        console.log("Notification permission granted.");
      }
    });
  };

  // @ts-ignore
  const firebase = (provider) => {
    const firebaseConfig = {
      apiKey: "AIzaSyA0GkXhwJMgW7L6giY7803Zq0GgBXcrZo8",
      authDomain: "kiwichat-aa07b.firebaseapp.com",
      projectId: "kiwichat-aa07b",
      storageBucket: "kiwichat-aa07b.appspot.com",
      messagingSenderId: "1039982538307",
      appId: "1:1039982538307:web:239e1460f6c54e5bb7eef6",
      measurementId: "G-X7CC3ER51Z",
    };
    const app = initializeApp(firebaseConfig);
    const messaging = getMessaging(app);

    // Get registration token. Initially this makes a network call, once retrieved
    // subsequent calls to getToken will return from cache.
    getToken(messaging, {
      vapidKey:
        "BP57nU_FQEFxjM2kYZm5Pug_nomX6vj_iAdkQVOK_9ptMuD3-7-5cLWvcKJf2ATpb5qDdIlpLCyodZuo4DdEX6E",
    })
      .then((currentToken) => {
        if (currentToken) {
          console.log({ currentToken });
          const cmd = UpdateUserNotificationDataCmd({
            id: "FNF^6969696969",
            fcmToken: currentToken,
            type: "fnf",
          //  iosDeviceToken: 'd9415be1d596bbdbe78691033df3dadabf4c34f0adaf0111334150972c29222a',
            appSource: AppSource.VIDEO,
          });
          provider.send(cmd).then((res: any) => console.log({ res }));
          // Send the token to your server and update the UI if necessary
          // ...
        } else {
          // Show permission request UI
          console.log(
            "No registration token available. Request permission to generate one."
          );
          // ...
        }
      })
      .catch((err) => {
        console.log("An error occurred while retrieving token. ", err);
        // ...
      });

    onMessage(messaging, (payload) => {
      console.log("onMessage", { payload });
    });
  };

  useEffect(() => {

    requestPermission();
    Auth.configure(config);
    const process = async () => {

/*      const kiwiTokenGetterFromRefresh = kiwiTokenSvc().kiwiTokenGetterFactory({
        cognitoClientId: config.Auth.userPoolWebClientId,
        cognitoRegion: "us-east-1",
      });
      const externalTokenFn = kiwiTokenGetterFromRefresh(
        verify.signInUserSession.refreshToken.token
      );*/
      const kiwiProvider = KiwiProvider({ config });
      const user  = await Auth.signIn('+11111111111');

      console.log({user})

      const verify = await Auth.sendCustomChallengeAnswer(user, '111111');



      kiwiProvider.auth.setUserCredentials({
        idToken: verify.signInUserSession.idToken.jwtToken,
        refreshToken: verify.signInUserSession.refreshToken.token,
        accessToken: verify.signInUserSession.accessToken.jwtToken
      }, setAuthLoading)
      console.log({verify})

      // @ts-ignore
      setProvider(kiwiProvider);
      firebase(kiwiProvider);
    }

    process();

  }, []);

  const setUserHandler = async (id: string) => {
    if (provider) {
      const kiwi = KiwiSvc(provider);
      const fnf = await kiwi.fnf.info({ id: id });
      // @ts-ignore
      setUser(fnf);
    }
  };

  const sendNotificationHandler = async (data: any) => {
    console.log(data);
    if (provider) {
      const kiwi = KiwiSvc(provider);
      const result = await kiwi.video.sendCallNotification(data);
      console.log(result);
    }
  };

  const handleConnectToCall = async (agoraChannelId: string) => {
    if (provider) {
      const kiwi = KiwiSvc(provider);
      const [channelId, messageId] = agoraChannelId.split("_");

      const data = {
        channelId,
        agoraChannelId: agoraChannelId,
        type: "Join",
        messageId: Number(messageId),
        // @ts-ignore
        senderId: user?.id,
      };
      const result = await kiwi.video.sendCallNotification(data);
      console.log(result);
      // @ts-ignore
      return result?.token;
    }
  };

  const handleFileInputChange = async (e: { target: { files: File[] } }) => {
    const b64image =
      "data:image/jpeg;base64,/9j//gAQTGF2YzU4LjU0LjEwMAD/2wBDAAgGBgcGBwgICAgICAkJCQoKCgkJCQkKCgoKCgoMDAwKCgoKCgoKDAwMDA0ODQ0NDA0ODg8PDxISEREVFRUZGR//xACAAAEBAQEBAQEBAAAAAAAAAAAAAQIDBAUGBwEBAQEBAAAAAAAAAAAAAAAAAAECAxABAAIBAwIDBgQDCAMBAAAAAAERAhITAxRhQQRRcQUxkSFSgdHBoWJCBhVDsSLwguEzs1OSMhEBAQEAAwEBAQAAAAAAAAAAABEBQRICIXEx/8AAEQgCgAFoAwEiAAIRAAMRAP/aAAwDAQACEQMRAD8A/k+Ge3lfhPxj1/14P2fuD39hnGPlfNZxOOVY8XNl/wCPl7/bl4vxdN8fJPFlcfWJ+mWM/DKO7G46v6X5nys8E3V4evp/w8/seH+nP6hwyxw8p5vLVxZf5OPly+uWEz8OPknxxn+XJ9Llw2+TLGPhEzH7sesi4wjQyrJTRQMqtAIKAgoCCgIigIUoCUKoMjQDNFNAMlNUAzSU2gIKoqFNAJRS0qjNFNCjNJTaUDNJTdAMVJTaUmjKN18ikH5zl/ov3fl/1cnmeCf4eTVHyyiXl5f6O8zj/wBXvHKvTk4on94l+vG++sx+Z8l/S/Jw3veZ13MXOONTUfy+ne36SI0xjFzNREXPx+nq0ibtMKRRFQVAFRQEVEAAAABFQAARVRRQFBBUARUAAAABVRQAFUBVEFQQAARVBkaoQEUEQUBBQERpEVBQEFAQUEQUBBUFAUEUAAAAAEVaBkpqkoEFAFFBBSlEFopRBaKBFKKAoWhBBaNICNUCIjSAiKAgoioKAgUoICiIjSAgoCKAoAAqNAgoCCgIKCIjSUKKUtAiighSrQM0U1RSjK0tLSjNFN0UyM0U1S6QYZaRUQVAQAUBUEFAQUBEaQRBUQAFAABUUUFoBBQRFAAFBBQAUQRQFVUVQFpaEZVaWhUWhoGaG6KB50aQRBQGRRVQVEAAQAARQEFoBBUABQRRQQUBBaAQUQRQUFRQUFBFFFAVQpVapNGKWmqWgZWIapQSIKaAeVFQQRQEFARGkBBQEFAQUBBRBEaFGRoBloUEFAQUBBQEUAAUBUaAFAFUpVWlpYhaEZaUoEpaaiFoVIhaaiFB4UVBERQEFEEFQAABFAQUBFAEUBAAUVFAAABQQUBBQEUUBRYAUaoEpVpVEaKWAKWIWIbiAZiGqaiFpFZopuigfNRpkQAUKRUARRAAAAARQRBUAAAABQBQAFBQRQEAUEFWgRqBRRoiGtKiQ1ELENxAM0ululiAZiG4hqMWoxBmlpvQukGKXS6RiaJB8ZJUBkWkQEVAABBFBUUBAABFQAFBFFAARRUUAFUQUEBVBlqhQKWIVYgGohYgiG4gCIahYhqIVSnTHExh3xwBzjFdLtGC7YjlGLWl2x478HTZB54wXRL0bfZdsH5YUQZFQEAABQRGkBBUABQQVEAFAAFUABUUAABUURQWFFgVaAhqIIhuIQXGHSISIbiFCIbiFiG4gDHF6cMGOPF6uPBoZjC3bHgt24+F7OPgvwB4sOB1jgfQx8u6bHYV8vZNqH0cuDszsdhH87RUZVABEFQBFAQUBBQEFQAFQQUBFAUVFRAABUUABRVhFhRtqGYagGobhluIBuG8WYbxUdIh1wxtjGLejjhUdOPB7eHity48X0ODD4CuvFwvbweXyzmIxiZODi1TEP1HkvKY+X44+n+eY+s+naGvOVN2PmcXufkmImYiPbP1/a3X+yMvXF9ga6Yz21+b5/KZcczEw4dPL9H5vijPiyv4xD5e1DO5Gs2v42y0jm0gAiIqAAAAAAAAAAIAAAACoqAqKACqIooAQsCLDcMw3CjUOkQxDpANQ6Yx9WYbx+Kjvx4vTx4uXH4PTx4ivRxQ+n5fF4OKH0vL+C4Pse7OKMuXC4+E2/RPge7+THj5MZn2PvRMTFw345/WPfCgznnjx4zllNRDe6y4+d5Y4+HK/jP0h8jfZ895yebKfSPhDw7rnvp085Mfy5FRzVAARFQAAQAAAFAAAEAABUVAAAVFBQBBUVQhpIagFhuGYagG4dIc4dIkG8ZdcPi5YuuKj04PThLyY5OuGaq+hxZvocOb4+Gb2cfKD7fDzU+jw+8eTiiruPSX5/j5nfqE/g+9l74zr6Y4vB5jzufL/wDrJ8+fMd3HPn7r93lZnGPRy8zjvPJnyzM/Fjc7qPxKKjKIACAAIqCAAACKKigAAAAAqCKACooAKIigorcMtQDUNQzDQNNQw1AOuLrjLhDpEg9GMumOX1cMZatc0evHJ3w5KeCMph1xzUfRx5W914I5F3BXu3XPLmebcZnOwdsuRnccdUpqlB+cZVAAAQAAAAAABEAAAAABVAQFRQAAFRQFhFgRpUUFahlqFGlhluAbhpiGgdIyatyiWrUdIydIycGomgemMmrcIya1CutluepdQrdlsajUg+CioqAAIAIAIAAAAAAACAACgCiooCooAAiqigNMtArUMwsA2sMqo3CswoNqyoNWsSyorcS1bENIrVls2A3qXVDmqD44I2iiAKyqIgAoAIAAAACoqAACgAKiiiooAACooiqgDSoCtNMqDUNQw0DSpAo2rLSDUKzCitCKAAg+QCNoqAIAIoAIAAAAAAAICooKAAAKAAqoogqKAqKKqooKqANNQyoNKkLCjUNMwqDcCKiqtooKIiD5KA6IAIAICiAiqyoKggKIAoioCooKAAqKAACgCqqKAqANKyoNKysA0qANKy1ANQrLSK1DTDQNKyCtICD5CCOjKiAiiAKIIKIAogCiAKIqAqKCqyoKrKgoigoAKqKKKigoCCgoLCsqDSsqDUNMrArULbNqK3BaQINQrEKo+TbKDbKiAKICKIWCiWWgtiAKIAqsqgqsqDQigogDQigKigrTKiqIoLYigoioNKgDQig0sIsCrCooNQIIrSWCj44g6MqMiDQyoioACs2oKrIg0MgNDKgrTIDSsqg0IA0MqDQgDS2yoNAgrSsqgogDTUSzCgpaArVtWwtg3bVsWWK6WM2A1Z9WbLQfIEsdWVEAUtEEasZEGlYUGhkBoZEGhloFVlQatbYVBoQBq1tksGrLZssG7W2LWxXSxhqwbGGgUtLAaVm2gURUVVZFG2nO2rFaJZsmQW1tiy2R8oYHVlstgBuy2FsRqy2bLBqy0EFtbZRBuy2FtRqy2bUGrW2GkGrLZUGhlUFEBWltkUbtWFsG1ti1tBuxmwG1ti1sGlthoGrVhoVbac2oBoZBW0QmUFLZstB8u0tiy3Vluy2CwbstiyxG7LYstBuy2LWwbstiywbsti1sGrLZssG7atztbQbtbYssVu1tiywbsti1tBuy2bLBuy2bLB0stiywdLW3O1sHSy2LLB0tbc7asVu1tztbB0tXO1sG7LYssVuy2UmQbs1doc7L7Sg+XZaxgmUTi6MlrbnaWDpZbnZYN2tsWWDdlsWWg6WWxZYjdlsWWK3a2xZYN2tsWWg6Wtudlg6WWxZYOlludrYOkStsRKoNWWyWDVtW52tg6WtudqDdrbnZYrpbVuVrYOlrbnZYOuo1Odlg66ltyssV01Fudlg1crqlz1UawfM624ueOY9kxLnyeexyr/JnFdr/AMHAWkx1jzfF6zHtxyj9Go8xwz/eY/N50mIn4xZSPXHJjPwmJ9kw1bwbWH2wm1j4XHsmfzKke+1t8/RMfDk5I/3y1G7H97l+MYz+hSPdZbxbnNH82M+3H/k3+ePjGE/OPzCPba28ceZz8eP5ZfnC9V68ecf/ADP6qkeuy3ljzeHjjyR/tn9LajzXF91e2Jj9Aei1twjzHFPwzx+bUZ4z8Mon8UHW1tytbB0s1MWWDpqW3K1sV0tdTnZYOsStuVrqB0tbcratB0tbcrWwdLXU52WDrZbna2DdtW5W1qBuy2NRYOlrbnZYrpZbGpNQOllueo1A1YxZYPlUjdJSqzSNUUgyNUUDNFNAM0lNoDNFNIDNJTdFA56Y9IZ28ftj5O1GkHHbjvHsyy/NYjLH4Z5x/uv/ABt1ooIzHJy/+yfxiPyhd7ljxxn8K/WTSaU+pGo8xn44x8/+Gup/gy/CmKSlI7dTx/xR+B1HH617YlxooI9Mc2M/zY/NqMo9YePTE+C6I9IKR7bXU8OmvhMx7Ja1Zx/PJTq9dmp5Nzk9Y/GF3eWPs+U/mtI9lmp5Y58vHGJ9ktdRHplAR6dS6nmjzGE+Ne2JhqObj+/H5iO+pdTlGUT8Jj5rqQddS6nHUahXbUupxjJdQOuo1OepNQOuo1OWoiQdNRqc9RqQeVGkVWRpAQUBEaARGgGRoBkaAZGgGRqigZGqKQZopqigZopqilGaKaoQZopVBiimwGKNLYKxpTRE+FulFIOW3Hp+i1lj8Mso/GXSkpRmMuX75+ULHLyx4xP4LSUI1v5/bj+B1E/b+7Ok0izG48xj4xk11GPf5OWlnSo9Ec2Pq1HJHq8mlNIPbqst4frHjPzLn7svmD1I0giI0gIKKIKUCC0tIMjVJQILRQILRQILRQILS0gyNUUDI1RQMjVFKrI1RSIyNUUoyq0CoKIIU1RQM0U1RQM0U1RQMUlOlJSjGlmnWoKBy0pp7OlFIO+3Kbb27XZNpUePbNt7Ns21Hj25Nt7No20Hj2123r2iOFR5dpdvs9UcK7XYHk2+xt9ns2uy7KK8W32Tae7ZXYB4dpdrs92wvTg8G12Nrs+h069OD5212XafQjy7XTIPm7Sbb6fSnTdgfM2qXafS6aW+ln0CPlbKbT63SdjpOxR8naNrs+t0nY6Tso+RtJtS+x0fY6OfQI+PtSm3L7PRz6J0U+iD4+3kaJfY6LL0TosvQHyNEpol9bop9GZ8ll6A+Xpk0y+lPk8vROjy9AfN0lPfPlKTpp9AeGke2fK5T4MdNlHgDzUPRsTHgk8IPPSU77Um1IPtbDOz2fS2U2VHzdnsmx2fS2eybIr52w1sPdsLHEDwbC7L37K7IPBs9l2Hv2V2AeDYNh9DYXZB8/Y7N7D3bLccKUfP2F6d9COJqOIpHz48v2a6fs+htdmo4k3R87pzp+0vpbRtJR4I8v2ajy737XZqOEqvB0yx5bs+hHDLWwUfP6ft+6x5bs+hstbJR87puzXTR6PpRwdl2b8Fo+Z0sfadLHo+rsQbPYHy+kj0XpY9H1NlrZB8rpI9Do49P2fW2Gtjsg+N0UeidFHp+z7Ox2a2OwPidD/DDHQR9v8Ar5PvbHZOn7A+D0Mfb/r5Mz5Dt+z7/Tr0wPz39nx6Oc+7o9H6Tpk6XsD8zl7t/hc8vdvZ+p6SGJ8pHopH5Sfdk9k/s2ez9VPk6TpAj5+3Jo7PXtm20jx6OxtPXtptivLtG09e2u0g8m2bb2bZtg8m2bb2bZtwDyaF23r2zbj0B5o42tp6NCxiyPPtNRxdno0wukHHbJ4nopaB544pbjDs7UtA5xxmiHRpBzjBrRDdNUK56WowhulBnQsYR6NwqjGhYwhtYgRnTC6GqaoGNC6GmgY0LGDa0gxtrtw2oOe1C7boorltwbUejsUDjtR6Jsw70UDzTwpsQ9WmDRAPhaTS3RTojGkpuikGKKbpaBzpdLdFIMUtNUUDNJTdJQM0tNUtIM0U1SqM0rRQJC0tFJoURC0qA0KKCwoEKKoNQjQCo0UFoEFpUhoBpFAVFBQhVEUURBQHxRoaGaKaKBmhqigZVaWgZFpQZSmwGSmhBmlUoEUUEUUBUVAUUCFAVVRQVUUGhAGlZaBYVAGlZUGoEAaW2VtBoZtVFEAfJFG0RQABQRQRQFBBQEWlBEpGgEUVBFFBBUBVRUUVFAVFBVRQUAFVAGltlVGrVhQaVhUGhlbEaGb+q2DQzag+YoNgAAKAigigKIigAKAigAqKKAAKigKioAACoA0IA0rKgolqCqyoKWgDVqwoNDKgvitsX9ZUG4ldTADwqitIAoqKKCCoAoIAAAAAACoqAAoKigKggogCiAKrKgoigqooCoA0IKKIApaHig0JYDQyoPGWiW0NWrNlg0tsWoNWMiDSsijQyWg0MqClolg1YzaoqrbKWDdozaoNWWyA00wWo2MWWI2MWWg2WzZYN2tsWWDaudrYNjFlitlsWtiN2mPwZmfoQitq5tRJui2WzZqhKP/Z";
    const arrayBuffer = decode(b64image.split(",")[1]);

    if (provider) {
      const cmd = AddVideoCallThumbnailCmd({
        agoraChannelId: "C^I^145047-FNF^1111111111_442",
        sid: "08e4cc47b84190673fd568895d8f5eaf",
        fnfThumbnailFile: arrayBuffer,
        inmateThumbnailFile: arrayBuffer,
      });
      // @ts-ignore
      const cmdRes = await provider.send(cmd);

      console.log({ cmdRes });
    }
  };

  return (
    <Grid container style={{ height: "100rem" }}>
      <input
        accept="image/*"
        id="raised-button-file"
        type="file"
        // @ts-ignore
        onChange={handleFileInputChange}
      />
      <Grid
        item
        xs={3}
        style={{
          padding: "20px",
          border: "1px solid lightgray",
          height: "100%",
        }}
      >
        <UserProfile
          handleIdUpdated={setUserHandler}
          user={user}
          handleSendNotification={sendNotificationHandler}
        />
      </Grid>
      <Grid item xs={6}>
        <Agora user={user} handleConnect={handleConnectToCall} />
      </Grid>
      <Grid item xs={3}>
        <CallSub user={user} provider={provider} />
      </Grid>
    </Grid>
  );
}

export default App;
