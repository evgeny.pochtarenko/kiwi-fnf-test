import {ExternalTokenInfo} from "kiwi-sdk";

function fetchOptions(cognito_client_id: string, refresh_token: string): RequestInit {
  return  {
    headers: {
      "X-Amz-Target": "AWSCognitoIdentityProviderService.InitiateAuth",
      "Content-Type": "application/x-amz-json-1.1",
    },
    mode: 'cors',
    cache: 'no-cache',
    method: 'POST',
    body: JSON.stringify({
      ClientId: cognito_client_id,
      AuthFlow: 'REFRESH_TOKEN_AUTH',
      AuthParameters: {
        REFRESH_TOKEN: refresh_token,
        //SECRET_HASH: "your_secret", // In case you have configured client secret
      }
    }),
  }
}

function isTokenValid({accessToken}: { accessToken: any }) {
  if(!accessToken) {
    return false;
  }
  const accessTokenPayload = JSON.parse(atob(accessToken.split(".")[1]));
  // console.log(accessTokenPayload.exp * 1000);
  return Date.now() < accessTokenPayload.exp * 1000;
}

function onGetTokenSuccess(res: any) {
  //console.log(res)
  if (!res.ok) {
    console.log("error getting token");
    return "";
  }
  return res.json();
}

function onGetTokenRejection(res: any) {
  console.log("rejection ==>", res);
  return "";
}
function onGetTokenError(e: any) {
  console.log("error getting token ==>", e);
  return "";
}

export interface cognitoOptionsInput {
  cognitoClientId: string,
  cognitoRegion?: string
}


const kiwiTokenGetterFactory = (cognitoOptions: cognitoOptionsInput) => (refreshToken: string) => {

  const {cognitoClientId, cognitoRegion} = cognitoOptions

  const cognitoCallOptions = fetchOptions(cognitoClientId, refreshToken);
  const cognitoUrl = `https://cognito-idp.${cognitoRegion}.amazonaws.com/`;
  const tokens: ExternalTokenInfo = {
    id: "",
    access: "",
    refresh: refreshToken
  }


  return async function  getToken(): Promise<ExternalTokenInfo> {

    console.log("getting token from cognito");
    console.log("cognitoCallOptions ==>", cognitoCallOptions);
    if (isTokenValid({accessToken: tokens.access})) {
      return Promise.resolve(tokens);
    };

    const rawData = await fetch(cognitoUrl, cognitoCallOptions);

    const data = await onGetTokenSuccess(rawData);

    if (!data) {
      return Promise.reject({
        id: "",
        access: "",
        refresh: refreshToken
      })

    }

  /*  console.log('Got Cognito tokens ====>', data);*/

    tokens.access = data.AuthenticationResult.AccessToken;
    tokens.id = data.AuthenticationResult.IdToken;

    return Promise.resolve({...tokens});
  }


}





export const kiwiTokenSvc = () => {
  return { kiwiTokenGetterFactory }
};