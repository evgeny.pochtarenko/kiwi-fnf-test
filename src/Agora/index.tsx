import {AgoraVideoPlayer, ClientConfig, createClient, createMicrophoneAndCameraTracks} from 'agora-rtc-react';
import React, {useEffect, useState} from 'react';
import {Button, TextField} from "@mui/material";
import classes from "./Agora.module.scss";

const config = {
  mode: 'rtc',
  codec: 'vp8',
};

const appId = '7b8c86e608fd4f4ab5432df4681d69aa'; //ENTER APP ID HERE
const useClient = createClient(config as ClientConfig);
const useMicrophoneAndCameraTracks = createMicrophoneAndCameraTracks();

// @ts-ignore
const Controls = ({tracks, setStart, setInCall}) => {
  const client = useClient();

  const [trackState, setTrackState] = useState({video: true, audio: true});

  const mute = async (type: string) => {
    if (type === 'audio') {
      await tracks[0].setEnabled(!trackState.audio);
      setTrackState((ps) => {
        return {...ps, audio: !ps.audio};
      });
    } else if (type === 'video') {
      await tracks[1].setEnabled(!trackState.video);
      setTrackState((ps) => {
        return {...ps, video: !ps.video};
      });
    }
  };

  const leaveChannel = async () => {
    await client.leave();
    client.removeAllListeners();
    tracks[0].close();
    tracks[1].close();
    setStart(false);
    setInCall(false);
  };

  return (
    <div className="controls">
      <p className={trackState.audio ? 'on' : ''} onClick={() => mute('audio')}>
        {trackState.audio ? 'MuteAudio' : 'UnmuteAudio'}
      </p>
      <p className={trackState.video ? 'on' : ''} onClick={() => mute('video')}>
        {trackState.video ? 'MuteVideo' : 'UnmuteVideo'}
      </p>
      {<p onClick={() => leaveChannel()}>Leave</p>}
    </div>
  );
};

// @ts-ignore
const Videos = ({users, tracks}) => {
  console.log({tracks});
  return (
    <div>
      <div id="videos" className={classes.videos}>
        <AgoraVideoPlayer className={classes.vid} videoTrack={tracks[1]}/>
        {users.length > 0 &&
          // @ts-ignore
          users.map((user) => {
            console.log('users', users);
            if (user.videoTrack) {
              return <AgoraVideoPlayer className={classes.vid} videoTrack={user.videoTrack} key={user.uid}/>;
            } else return null;
          })}
      </div>
    </div>
  );
};

// @ts-ignore
const VideoCall = ({setInCall, channelName, authToken, authUser}) => {
  console.log({channelName}, {authToken});
  const [users, setUsers] = useState([]);
  const [start, setStart] = useState(false);
  const client = useClient();
  const {ready, tracks} = useMicrophoneAndCameraTracks();

  useEffect(() => {
    // function to initialise the SDK
    let init = async (name: any) => {
      console.log(name);
      client.on('user-published', async (user: { audioTrack: { play: () => void; }; }, mediaType: string) => {
        console.log(user);
        // @ts-ignore
        await client.subscribe(user, mediaType);
        console.log('subscribe success');
        if (mediaType === 'video') {
          // @ts-ignore
          setUsers((prevUsers) => {
            return [...prevUsers, user];
          });
        }
        if (mediaType === 'audio') {
          user.audioTrack?.play();
        }
      });
// @ts-ignore
      client.on('user-unpublished', (user, type) => {
        console.log('unpublished', user, type);
        if (type === 'audio') {
          user.audioTrack?.stop();
        }
        if (type === 'video') {
          setUsers((prevUsers) => {
            // @ts-ignore
            return prevUsers.filter((User) => User.uid !== user.uid);
          });
        }
      });
// @ts-ignore
      client.on('user-left', (user) => {
        console.log('leaving', user);
        setUsers((prevUsers) => {
          // @ts-ignore
          return prevUsers.filter((User) => User.uid !== user.uid);
        });
      });

      let q = await client.join(appId, name, authToken, authUser.id);
      console.log({q})
      if (tracks) await client.publish([tracks[0], tracks[1]]);
      setStart(true);
    };
    console.log(1111, ready, tracks);
    if (ready && tracks) {
      console.log('init ready', channelName);
      init(channelName);
    }
  }, [channelName, client, ready, tracks]);

  return (
    <div className={classes.App}>
{/*      {ready && tracks && <Controls tracks={tracks} setStart={setStart} setInCall={setInCall}/>}*/}
      {start && tracks && <Videos users={users} tracks={tracks}/>}
    </div>
  );
};

// @ts-ignore
const ChannelForm = ({setInCall, setChannelName, user, setAuthToken, connectToCall}) => {

  const [channel, setChannel] = useState('')

  const handleJoin = async (e: any) => {
    e.preventDefault();
    setChannelName(channel);
    const token = await connectToCall(channel);
    console.log({token})
    setAuthToken(token);
    setInCall(true)
  }

  return (
    <form onSubmit={handleJoin} className={classes.join}>
      <TextField  onChange={(e) => {
        setChannel(e.target.value)
      }} id="outlined-basic" label="Agora Channel Id" variant="outlined" style={{marginRight: "20px"}}/>

      <Button type={'submit'} className={classes.button} variant="contained">Join</Button>
    </form>
  );
};
// @ts-ignore
const Agora = ({user, handleConnect}) => {
  const [inCall, setInCall] = useState(false);
  const [channelName, setChannelName] = useState('');
  const [authToken, setAuthToken] = useState('');

  return (
    <div>
      <h1 className="heading">Agora RTC NG SDK React Wrapper</h1>
      {inCall ? (
        <VideoCall authUser={user} setInCall={setInCall} channelName={channelName} authToken={authToken}/>
      ) : (
        <ChannelForm user={user} setInCall={setInCall} setChannelName={setChannelName} setAuthToken={setAuthToken} connectToCall={handleConnect}/>
      )}
    </div>
  );
};
export default Agora;
