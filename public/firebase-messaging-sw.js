// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/9.0.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/9.0.0/firebase-messaging-compat.js');

// Initialize the Firebase app in the service worker by passing the generated config
const firebaseConfig = {
  apiKey: "AIzaSyA0GkXhwJMgW7L6giY7803Zq0GgBXcrZo8",
  authDomain: "kiwichat-aa07b.firebaseapp.com",
  projectId: "kiwichat-aa07b",
  storageBucket: "kiwichat-aa07b.appspot.com",
  messagingSenderId: "1039982538307",
  appId: "1:1039982538307:web:239e1460f6c54e5bb7eef6",
  measurementId: "G-X7CC3ER51Z"
};

firebase.initializeApp(firebaseConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();
console.log({messaging})
messaging.onBackgroundMessage(function(payload) {
  console.log('Received background message ', payload);

  const notificationTitle = payload.notification.title;
  const notificationOptions = {
    body: payload.notification.body,
    actions: [
      { action: "exampleAction", title: "Get now."}],
  };

  self.registration.showNotification(notificationTitle,
    notificationOptions);

});