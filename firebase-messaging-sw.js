import { getMessaging } from "firebase/messaging";

import { onBackgroundMessage } from "firebase/messaging/sw";

import {initializeApp} from "firebase/app";
c
onst firebaseConfig = {
  apiKey: "AIzaSyA0GkXhwJMgW7L6giY7803Zq0GgBXcrZo8",
  authDomain: "kiwichat-aa07b.firebaseapp.com",
  projectId: "kiwichat-aa07b",
  storageBucket: "kiwichat-aa07b.appspot.com",
  messagingSenderId: "1039982538307",
  appId: "1:1039982538307:web:239e1460f6c54e5bb7eef6",
  measurementId: "G-X7CC3ER51Z"
};
const app = initializeApp(firebaseConfig);
const messaging = getMessaging(app);

onBackgroundMessage(messaging, (payload) => {
  console.log('[firebase-messaging-sw.js] Received background message ', payload);
  // Customize notification here
  const notificationTitle = 'Background Message Title';
  const notificationOptions = {
    body: 'Background Message body.',
    icon: '/firebase-logo.png'
  };

  self.reg ration.showNotification(notificationTitle,
    notificationOptions);
});